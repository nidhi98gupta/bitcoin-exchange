package com.btcWalletPay.arbitrage;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.Pair;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by nidhi on 23/7/17.
 */

public class UpdatePrices extends Service {

    public  ResultReceiver receiver;
    public Double profit;
    public Double reqProfit;

    private int count;

    private final double hdfcEurToInr = 85.7d;//TODO make it configurable / user input
    private final double InrChargesPer3500EurOutwardRemittance = 1500d;
    private final double eurChargesPer3500EurOutwardRemittance = InrChargesPer3500EurOutwardRemittance/3500d;

    private final double eurToInr = hdfcEurToInr + eurChargesPer3500EurOutwardRemittance;

    private final double inrToEur = 1/eurToInr;

    private List<Pair<Double,Double>> BidsList ;
    private List<Pair<Double,Double>> AsksList;
    private List<Pair<Double,Double>> Profit;
    private Looper mServiceLooperCoinsecure;
    private  Looper mServiceLooperCoinfloor;
    private ServiceHandlerCoinsecure mServiceHandlerCoinsecure;
    private  ServiceHandlerCoinfloor mServiceHandlerCoinfloor;
    // Handler that receives messages from the thread
    private final class ServiceHandlerCoinsecure extends Handler {
        public ServiceHandlerCoinsecure(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            try {
                Thread.sleep(10000);
                Log.e("1","In Coinsecure");
                URL url = new URL("https://api.coinsecure.in/v1/exchange/bid/orders");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(false);

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    coinsecureResult(sb.toString());
                    Message msgCoinfloor = mServiceHandlerCoinfloor.obtainMessage();
                    msgCoinfloor.arg1 = 2;
                    mServiceHandlerCoinfloor.sendMessage(msgCoinfloor);

                    // return sb.toString();

                } else {
                    //  returhttp reequest every 10 seconds androidn new String("false : "+responseCode);
                }

            } catch (Exception e) {
                // return new String("Exception: " + e.getMessage());
            }




            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
           // stopSelf(msg.arg1);
        }
    }
    private final class ServiceHandlerCoinfloor extends Handler {
        public ServiceHandlerCoinfloor(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            try{
                Log.e("1","In Coinfloor");
                URL url=new URL("https://webapi.coinfloor.co.uk:8090/bist/XBT/GBP/order_book/");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(false);

                int responseCode=conn.getResponseCode();


                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    coinfloorResult(sb.toString());
                    //return sb.toString();

                }
                else {
                  //  return new String("false : "+responseCode);
                }

            }
            catch(Exception e){
                //return new String("Exception: " + e.getMessage());
            }
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        Log.e("1","Service Created");
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("CoinsecureThread",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooperCoinsecure = thread.getLooper();
        mServiceHandlerCoinsecure = new ServiceHandlerCoinsecure(mServiceLooperCoinsecure);

        HandlerThread thread1=new HandlerThread("CoinfloorThread",Process.THREAD_PRIORITY_BACKGROUND);
        thread1.start();
        mServiceLooperCoinfloor=thread1.getLooper();
        mServiceHandlerCoinfloor=new ServiceHandlerCoinfloor(mServiceLooperCoinfloor);





    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("OnStart","Service started");
        receiver = intent.getParcelableExtra("receiver");
        startForeground(1,new Notification());
        Message msgCoinsecure = mServiceHandlerCoinsecure.obtainMessage();
        msgCoinsecure.arg1 = startId;
        mServiceHandlerCoinsecure.sendMessage(msgCoinsecure);


        return START_STICKY;
        //return START_REDELIVER_INTENT;
    }

    public void coinsecureResult(String result){
        try {
            Log.e("result 1",result.substring(0,60));
            JSONObject json = new JSONObject(result);;
            JSONArray arr=json.getJSONArray("message");
            BidsList=new ArrayList<Pair<Double,Double>>();
            for(int i=0;i<arr.length();i++) {
                Pair<Double, Double> pair = new Pair<Double, Double>(arr.getJSONObject(i).getDouble("rate") * inrToEur / 100, arr.getJSONObject(i).getDouble("vol") / 100000000.0);
                BidsList.add(i, pair);
            }

        } catch (Exception e) {
            Log.e("Exception",e.getMessage());
        }
    }

    public void coinfloorResult(String result){
        Log.e("result 2",result.substring(0,60));
        try {

            JSONObject json = new JSONObject(result);
            JSONArray jsonArray=json.getJSONArray("asks");
            AsksList=new ArrayList<Pair<Double,Double>>() ;
            for(int i=0;i<jsonArray.length();i++){
                JSONArray jo=jsonArray.getJSONArray(i);
                Pair<Double,Double> pair=new Pair<>(jo.getDouble(0),jo.getDouble(1));
                AsksList.add(i,pair);
            }
            CalculateProfit(BidsList,AsksList);

        }
        catch (Exception e){

        }

        Message msgCoinsecure = mServiceHandlerCoinsecure.obtainMessage();
        msgCoinsecure.arg1 = 3;
        mServiceHandlerCoinsecure.sendMessage(msgCoinsecure);
    }


    public void CalculateProfit(List<Pair<Double,Double>> BidsList,List<Pair<Double,Double>> AsksList)
    {
        Profit=new ArrayList<Pair<Double, Double>>();
        Pair<Double,Double> profitPair;
        double bitcoins;
        int m=0,n=0;
        for(int i=0;i<10;i++){
            if(BidsList.get(m).second>AsksList.get(n).second){
                bitcoins= AsksList.get(n).second;
                profit=profitCalculator(BidsList.get(m).first,AsksList.get(n).first);
                Pair<Double,Double> pair=new Pair<>(BidsList.get(m).first,BidsList.get(m).second-AsksList.get(n).second);
                BidsList.set(m,pair);
                n++;

            }
            else
            {
                bitcoins= BidsList.get(m).second;
                profit=profitCalculator(BidsList.get(m).first,AsksList.get(n).first);
                Pair<Double,Double> pair=new Pair<>(AsksList.get(n).first,AsksList.get(n).second-BidsList.get(m).second);
                AsksList.set(n,pair);
                m++;

            }


            profitPair =new Pair<>(bitcoins,profit);
            Profit.add(i,profitPair);

        }
        notifyUser(Profit.get(0).second);
        Log.e("Profit",Profit.toString().substring(0,60)) ;
        Bundle bundle=new Bundle();
        //bundle.putParcelableArrayList("ProfitList",Profit);
        bundle.putSerializable("ProfitList",(Serializable)Profit);
        Log.e("zzzzz","Approaching receiver");
        receiver.send(13,bundle);
        Log.e("zzzz","Jumped receiver");

    }
    public Double profitCalculator(Double sellPrice,Double buyPrice){
        return ((sellPrice*0.996-buyPrice*1.004)*100)/(buyPrice*1.004);
    }


    public void notifyUser(Double profit) {
        Log.e("1","Notifying User");
        Intent resultIntent = new Intent(this, MainActivity.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        1,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle("Profit = "+profit)
                        .setContentText(formattedDate)
                        .setContentIntent(resultPendingIntent)
                        .setOngoing(true);


        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        reqProfit=10d;
        /*
        try {
            reqProfit=Double.parseDouble(sharedPref.getString("ReqProfit", "10"));
        }
        catch (NumberFormatException ex){
            reqProfit=10d;
        }
        */
        if(profit>=reqProfit)
            mBuilder.setSound(soundUri);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(1, mBuilder.build());

    }

}
