package com.btcWalletPay.arbitrage;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.Pair;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity implements BitcoinPriceReceiver.Receiver {

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    public Double profit;
    public Double reqProfit;
    private ProgressBar prog;
    private int count;
    private EditText editProfitLimit;

    private final double hdfcEurToInr = 85.7d;//TODO make it configurable / user input
    private final double InrChargesPer3500EurOutwardRemittance = 1500d;
    private final double eurChargesPer3500EurOutwardRemittance = InrChargesPer3500EurOutwardRemittance/3500d;

    private final double eurToInr = hdfcEurToInr + eurChargesPer3500EurOutwardRemittance;

    private final double inrToEur = 1/eurToInr;

    private List<Pair<Double,Double>> BidsList ;
    private List<Pair<Double,Double>> AsksList;
    private List<Pair<Double,Double>> Profit;


    private BitcoinPriceReceiver mReceiver;
    //spr.getStatus()
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("inrToeur","Here"+inrToEur);
        setContentView(R.layout.activity_main);
        count=0;
        //onNewIntent(getIntent());

        sharedPref = getPreferences(Context.MODE_PRIVATE);
        prog =(ProgressBar)findViewById(R.id.progressBar);
       // prog.setEnabled(false);
        editProfitLimit=(EditText)findViewById(R.id.editProfitLimit);
        editProfitLimit.setText(sharedPref.getString("ReqProfit", "10"));
        editProfitLimit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                editor = sharedPref.edit();
                editor.putString("ReqProfit",editProfitLimit.getText().toString());
                editor.commit();

            }
        });
        mReceiver = new BitcoinPriceReceiver(new Handler());
        mReceiver.setReceiver(this);

        Intent intent = new Intent(this, UpdatePrices.class);
        intent.putExtra("receiver", mReceiver);
        startService(intent);
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        Log.e("1","Recieved bundle");
        Profit=(ArrayList<Pair<Double,Double>>)resultData.getSerializable("ProfitList");
        TextView textBox=(TextView)(findViewById(R.id.InfoBox));

        textBox.setText("Bitcoins Transaction       Profit Percentage");
        for(int i=0;i<10;i++){
            String bitcoinsToString = String.format("%.3f", Profit.get(i).first);
            String profitToString = String.format("%.2f", Profit.get(i).second);
            textBox.append("\n  "+bitcoinsToString+"                                "+profitToString);
        }


    }
}
